﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameSettings
{
    public enum BallSpeeds
    {
        Slow,
        Normal,
        Fast
    }

    public enum PaddleSizes
    {
        Small,
        Normal,
        Big
    }

    public static int _playerLifes;
    public static BallSpeeds _ballSpeed;
    public static PaddleSizes _paddleSize;
    public static float _paddleMovementSpeed;
    public static bool _multiball;
    public static int _simultaneousBallNb;
}
