﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpManager : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> _pickUpsPool;
    [SerializeField]
    private List<PickUpSO> _pickUpSOArray;
    [SerializeField]
    private GameObject _pickUpPrefab;

    public delegate void ApplyPickUpBonus(PickUpSO.PickUpType type, float value, float duration);
    public event ApplyPickUpBonus OnApplyPickUpBonus;

    private void Start()
    {
        for(int i = 0; i < _pickUpsPool.Count; i++)
        {
            _pickUpsPool[i].GetComponent<PickUp>().OnDeactivatePickUp += DeactivatePickUp;
            _pickUpsPool[i].GetComponent<PickUp>().OnPickedUp += PickedUp;
        }
    }

    private void OnDestroy()
    {
        for (int i = 0; i < _pickUpsPool.Count; i++)
        {
            _pickUpsPool[i].GetComponent<PickUp>().OnDeactivatePickUp -= DeactivatePickUp;
            _pickUpsPool[i].GetComponent<PickUp>().OnPickedUp -= PickedUp;
        }
    }

    private void DeactivatePickUp(GameObject pickUpGO)
    {
        pickUpGO.transform.localPosition = Vector3.zero;
        pickUpGO.SetActive(false);
    }

    private void PickedUp(PickUpSO.PickUpType type, float value, float duration)
    {
        if (OnApplyPickUpBonus != null)
            OnApplyPickUpBonus(type, value, duration);
    }

    public void RandomizePickUpSpawn(Vector3 spawnPos)
    {
        int pickUpSoIndex = Random.Range(0, _pickUpSOArray.Count);

        GameObject pickUp = GetPooledPickUp();
        pickUp.transform.position = spawnPos;
        
        //fill the pick up with its SO data
        pickUp.GetComponent<PickUp>().Fill(_pickUpSOArray[pickUpSoIndex]);
        pickUp.SetActive(true);
    }

    private GameObject GetPooledPickUp()
    {
        //look for the first object in the pool that is not already in use
        for (int i = 0; i < _pickUpsPool.Count; i++)
        {
            if (!_pickUpsPool[i].activeInHierarchy)
            {
                return _pickUpsPool[i];
            }
        }

        //if all objects of the pool are in use, spawn a new one and add it to the pool
        GameObject pickUp = Instantiate(_pickUpPrefab, Vector3.zero, Quaternion.identity);
        pickUp.GetComponent<PickUp>().OnDeactivatePickUp += DeactivatePickUp;
        pickUp.GetComponent<PickUp>().OnPickedUp += PickedUp;
        pickUp.transform.SetParent(this.transform);
        _pickUpsPool.Add(pickUp);

        return pickUp;
    }
}
