﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    [SerializeField]
    private float _ballSpeed = 7f;
    [SerializeField]
    private int _ballIndex = 0;
    private Vector3 _startMovementDirection = new Vector3(0.5f, -0.5f, 0f);
    private Vector3 _movementDirection;
    [SerializeField]
    private float _timeScale = 1f;

    public delegate void BallOutOfBounds(int ballIndex);
    public event BallOutOfBounds OnBallOutOfBounds;

    public delegate void BounceOnBorder(int audioSourceIndex);
    public event BounceOnBorder OnBounceOnBorder;

    public delegate void BounceOnPaddle(int audioSourceIndex);
    public event BounceOnPaddle OnBounceOnPaddle;

    public delegate void BounceOnBrick(int audioSourceIndex);
    public event BounceOnBrick OnBounceOnBrick;

    public void SetTimeScale(float timeScale)
    {
        _timeScale = timeScale;
    }

    public void SetSpeed(float speed)
    {
        _ballSpeed = speed;
    }

    public float GetSpeed()
    {
        return _ballSpeed;
    }

    public void SetIndex(int index)
    {
        _ballIndex = index;
    }

    public int GetIndex()
    {
        return _ballIndex;
    }

    public void Spawn(bool multiballMode, Vector3 spawnPos)
    {
        transform.position = spawnPos;
        _startMovementDirection = new Vector3(Random.Range(-0.5f, 0.5f), -1f, 0f);
        _startMovementDirection.Normalize();
    }

    private void OnEnable()
    {
        _movementDirection = _startMovementDirection;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += _movementDirection * _ballSpeed * _timeScale;
    }

    private void OnCollisionEnter(Collision collision)
    {
        _movementDirection = Vector3.Reflect(_movementDirection, collision.GetContact(0).normal);

        switch (collision.gameObject.layer)
        {
            case 9:
                if (OnBounceOnBorder != null)
                    OnBounceOnBorder(2);
                break;
            case 10:
                if (OnBounceOnPaddle != null)
                    OnBounceOnPaddle(3);
                break;
            case 12:
                if (OnBounceOnBrick != null)
                    OnBounceOnBrick(1);
                break;
            default:
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 11)
        {
            if (OnBallOutOfBounds != null)
                OnBallOutOfBounds(_ballIndex);
        }
    }
}
