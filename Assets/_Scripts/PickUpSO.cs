﻿using UnityEngine;

[CreateAssetMenu(fileName = "PickUp", menuName = "PickUp", order = 1)]
public class PickUpSO : ScriptableObject
{
    public enum PickUpType
    {
        bonusLife,
        timeScale,
        paddleScale,
        ballSpeed
    }
    public PickUpType _type;
    public float _value;
    public float _duration;

    public Mesh _visual;
    public Material _visualMaterial;
    public GameObject _pickedUpVFX;
}
