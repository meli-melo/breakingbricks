﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _playExitView;
    [SerializeField]
    private GameObject _settingsView;

    private void Start()
    {
        _playExitView.SetActive(true);
        _settingsView.SetActive(false);
    }

    public void ExitButtonClick()
    {
        Application.Quit();
    }

    public void PlayButtonClick()
    {
        _playExitView.SetActive(false);
        _settingsView.SetActive(true);
    }

    public void BackButtonClick()
    {
        _playExitView.SetActive(true);
        _settingsView.SetActive(false);
    }

    public void StartButtonClick()
    {
        // Use a coroutine to load the Scene in the background
        StartCoroutine(LoadYourAsyncScene());
    }

    IEnumerator LoadYourAsyncScene()
    {

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("GameScene");

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
