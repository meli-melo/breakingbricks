﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BricksManager : MonoBehaviour
{
    [SerializeField]
    private int _nbBricks = 50;
    [SerializeField]
    private int _bricksPerLine = 10;
    [SerializeField]
    private GameObject _brickPrefab;
    [SerializeField]
    private List<GameObject> _bricksArray = new List<GameObject>();

    public delegate void AllBricksDestroyed();
    public event AllBricksDestroyed OnAllBricksDestroyed;

    public delegate void BrickDestroyed(Vector3 brickPos);
    public event BrickDestroyed OnBrickDestroyed;

    private void destroyBrick(GameObject brickGo)
    {
        bool found = false;
        int index = 0;
        while(!found)
        {
            if(_bricksArray[index] == brickGo)
            {
                found = true;
            }
            else
            {
                index++;
            }
        }
        _bricksArray[index].GetComponent<Brick>().OnBrickCollided -= destroyBrick;
        Vector3 brickPos = _bricksArray[index].transform.position;
        Destroy(_bricksArray[index], 0.1f);
        _bricksArray.RemoveAt(index);

        if(_bricksArray.Count <= 0)
        {
            if (OnAllBricksDestroyed != null)
                OnAllBricksDestroyed();
        }
        else
        {
            if (OnBrickDestroyed != null)
                OnBrickDestroyed(brickPos);
        }
    }

    public void BuildBricks()
    {
        bool continueBuilding = true;
        int lineIndex = 0;
        while(continueBuilding)
        {
            continueBuilding = BuildBrickLine(lineIndex);
            lineIndex++;
        }
    }

    private bool BuildBrickLine(int lineIndex)
    {
        float horizontalSpacing = 22f / (_bricksPerLine - 1);

        for (int i = 0; i < _bricksPerLine; i++)
        {
            if(_bricksArray.Count < _nbBricks)
            {
                GameObject brick = Instantiate(_brickPrefab, this.transform) as GameObject;
                brick.transform.localPosition = new Vector3(-11f + i * horizontalSpacing, 6 - lineIndex * 1.1f, 0f);
                brick.GetComponent<Brick>().OnBrickCollided += destroyBrick;
                brick.layer = 12;
                _bricksArray.Add(brick);
            }
            else
            {
                return false;
            }
        }
        return true;
    }
}
