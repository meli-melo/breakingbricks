﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour
{
    [SerializeField]
    private float _speed = 1f;

    [SerializeField]
    private bool _move = false;
    private float _lerpPct = 0f;

    public float _movementLimits = 0f;
    private Vector3 _startPosition = new Vector3(0f, -7.6f, 0f);

    private void Start()
    {
        CalculateMovementLimits();
    }

    public void CalculateMovementLimits()
    {
        //calculating the left/right movement limits
        //side borders position minus their thickness and half the paddle size
        _movementLimits = 14 - (0.5f + transform.localScale.x / 2);
        if (this.transform.position.x > _movementLimits)
            this.transform.position = new Vector3(_movementLimits, this.transform.position.y, 0f);

        if (this.transform.position.x < -_movementLimits)
            this.transform.position = new Vector3(-_movementLimits, this.transform.position.y, 0f);
    }

    public void Respawn()
    {
        transform.position = _startPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if(!_move)
        {

#if UNITY_STANDALONE || UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _move = true;
                _lerpPct = 0f;
            }
#elif UNITY_ANDROID || UNITY_IOS
            if (Input.touchCount > 0)
            {
                _move = true;
                _lerpPct = 0f;
            }
#endif
        }


        if (_move)
        {
#if UNITY_STANDALONE || UNITY_EDITOR
            if (!Input.GetKey(KeyCode.Mouse0))
            {
                _move = false;
            }
            else
            {
                Vector3 mousePos = Input.mousePosition;
                mousePos = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, Camera.main.nearClipPlane));

                if (mousePos.x > _movementLimits)
                    mousePos.x = _movementLimits;
                else if (mousePos.x < -_movementLimits)
                    mousePos.x = -_movementLimits;

                mousePos.z = 0f;
                mousePos.y = transform.position.y;
                transform.position = new Vector3(Mathf.Lerp(transform.position.x, mousePos.x, _lerpPct), mousePos.y, mousePos.z);
                _lerpPct += Time.deltaTime * _speed;

            }

#elif UNITY_ANDROID || UNITY_IOS
            if (Input.touchCount <= 0)
            {
                _move = false;
            }
            else
            {
                Touch touch = Input.GetTouch(0);
                Vector3 touchWorldPos = Camera.main.ScreenToWorldPoint(touch.position);

                if (touchWorldPos.x > _movementLimits)
                    touchWorldPos.x = _movementLimits;
                else if (touchWorldPos.x < -_movementLimits)
                    touchWorldPos.x = -_movementLimits;

                touchWorldPos.z = 0f;
                touchWorldPos.y = transform.position.y;
                transform.position = new Vector3(Mathf.Lerp(transform.position.x, touchWorldPos.x, _lerpPct), touchWorldPos.y, touchWorldPos.z);
                _lerpPct += Time.deltaTime * _speed;
            }
#endif
        }
    }
}
