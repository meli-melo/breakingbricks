﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    [SerializeField]
    private int _brickIndex = 0;

    public delegate void BrickCollided(GameObject brickGo);
    public event BrickCollided OnBrickCollided;

    public void SetIndex(int index)
    {
        _brickIndex = index;
    }

    public int GetIndex()
    {
        return _brickIndex;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (OnBrickCollided != null)
            OnBrickCollided(this.gameObject);
    }
}
