﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    [SerializeField]
    private GameObject _visual;
    [SerializeField]
    private GameObject _pickedUpVFX;
    [SerializeField]
    private PickUpSO.PickUpType _type;
    [SerializeField]
    private float _value;
    [SerializeField]
    private float _duration;

    public delegate void PickedUp(PickUpSO.PickUpType type, float value, float duration);
    public event PickedUp OnPickedUp;

    public delegate void DeactivatePickUp(GameObject pickUpGO);
    public event DeactivatePickUp OnDeactivatePickUp;

    public void Fill(PickUpSO pickUpSO)
    {
        _visual.GetComponent<MeshFilter>().mesh = pickUpSO._visual;
        _visual.GetComponent<MeshRenderer>().material = pickUpSO._visualMaterial;
        _pickedUpVFX = Instantiate(pickUpSO._pickedUpVFX);
        _pickedUpVFX.transform.SetParent(this.transform);
        _pickedUpVFX.transform.localPosition = Vector3.zero;
        _pickedUpVFX.SetActive(false);
        _type = pickUpSO._type;
        _value = pickUpSO._value;
        _duration = pickUpSO._duration;

        //reactivate components (they get deactivated when object is picked up by player)
        Rigidbody rg = GetComponent<Rigidbody>();
        rg.useGravity = true;
        rg.mass = Random.Range(0.5f, 1f);
        rg.velocity = Vector3.zero;
        _visual.SetActive(true);
        _pickedUpVFX.SetActive(false);
        GetComponent<BoxCollider>().enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 10)
        {
            Rigidbody rg = GetComponent<Rigidbody>();
            rg.useGravity = false;
            rg.velocity = Vector3.zero;
            _visual.SetActive(false);
            _pickedUpVFX.SetActive(true);
            GetComponent<BoxCollider>().enabled = false;
            if (OnDeactivatePickUp != null)
                Invoke("CallDeactivate", 1.5f);

            if (OnPickedUp != null)
                OnPickedUp(_type, _value, _duration);
        }
        else if(other.gameObject.layer == 13)
        {
            if (OnDeactivatePickUp != null)
                OnDeactivatePickUp(this.gameObject);
        }
    }

    private void CallDeactivate()
    {
        if (OnDeactivatePickUp != null)
            OnDeactivatePickUp(this.gameObject);
    }
}
