﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneSoundManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource[] _audioSourcesArray;

    [SerializeField]
    private List<BallController> _ballControllers;
    [SerializeField]
    private PaddleController _paddleController;

    public void SetBallControllersList(List<BallController> ballControllersList)
    {
        for(int i =0; i < ballControllersList.Count; i++)
        {
            _ballControllers.Add(ballControllersList[i]);
            _ballControllers[i].OnBounceOnBorder += PlayAudio;
            _ballControllers[i].OnBounceOnBrick += PlayAudio;
            _ballControllers[i].OnBounceOnPaddle += PlayAudio;
        }
    }

    private void OnDestroy()
    {

        for (int i = 0; i < _ballControllers.Count; i++)
        {
            _ballControllers[i].OnBounceOnBorder -= PlayAudio;
            _ballControllers[i].OnBounceOnBrick -= PlayAudio;
            _ballControllers[i].OnBounceOnPaddle -= PlayAudio;
        }
    }

    private void PlayAudio(int audioSourceIndex)
    {
        _audioSourcesArray[audioSourceIndex].Play();
    }

    public void PlayStopBackgroundAudio(bool play)
    {
        if (play)
            _audioSourcesArray[0].Play();
        else
            _audioSourcesArray[0].Stop();
    }

    public void PlayLoseAudio()
    {
        _audioSourcesArray[5].Play();
    }

    public void PlayWinAudio()
    {
        _audioSourcesArray[6].Play();
    }

    public void PlayOutOfBoundsAudio()
    {
        _audioSourcesArray[7].Play();
    }
}
