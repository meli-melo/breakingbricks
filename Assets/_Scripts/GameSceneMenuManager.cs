﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneMenuManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _pauseMenu;
    [SerializeField]
    private GameObject _pauseButton;
    [SerializeField]
    private bool _pauseMenuOpened;

    public delegate void PauseMenuOpenClose(bool open);
    public event PauseMenuOpenClose OnPauseMenuOpenClose;

    // Start is called before the first frame update
    void Start()
    {
        _pauseButton.SetActive(true);
        _pauseMenu.SetActive(false);
    }

    public void PauseButtonClick()
    {
        _pauseMenuOpened = true;
        _pauseButton.SetActive(false);
        _pauseMenu.SetActive(true);

        if (OnPauseMenuOpenClose != null)
            OnPauseMenuOpenClose(_pauseMenuOpened);
    }

    public void ResumeButtonClick()
    {
        _pauseMenuOpened = false;
        _pauseMenu.SetActive(false);
        _pauseButton.SetActive(true);

        if (OnPauseMenuOpenClose != null)
            OnPauseMenuOpenClose(_pauseMenuOpened);
    }

    public void MainMenuButtonClick()
    {
        // Use a coroutine to load the Scene in the background
        StartCoroutine(LoadSceneAsync());
    }

    IEnumerator LoadSceneAsync()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("MainMenu");

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
