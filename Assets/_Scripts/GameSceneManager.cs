﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneManager : MonoBehaviour
{
    [System.Serializable]
    private struct GameData
    {
        public int _playerLifesLeft;
        public float _ballSpeed;
        public float _paddleSize;
        public float _paddleMovementSpeed;
        public bool _multiball;
        public int _simultaneousBallNb;
    }
    [SerializeField]
    private GameData _gameData;

    [SerializeField]
    private PaddleController _paddleController;
    [SerializeField]
    private GameObject _ballPrefab;
    [SerializeField]
    private List<BallController> _ballControllersArray;
    [SerializeField]
    private List<Transform> _ballSpawnPositions;
    //[SerializeField]
    //private GameObject _pickUpPrefab;
    //[SerializeField]
    //private List<PickUpSO> _pickUpSOArray;
    [SerializeField]
    private BricksManager _brickManager;
    [SerializeField]
    private GameSceneSoundManager _soundManager;
    [SerializeField]
    private PickUpManager _pickUpManager;
    [SerializeField]
    private GameSceneMenuManager _menuManager;
    [SerializeField]
    private bool _gameStarted = false;
    [SerializeField]
    private bool _isTimeScale = false;
    [SerializeField]
    private bool _isPaddleScale = false;
    [SerializeField]
    private bool _isBallSpeed = false;
    [SerializeField]
    private float _timeScaleBeforePause = 1f;

    // Start is called before the first frame update
    void Start()
    {
        FillGameData();

        _brickManager.OnAllBricksDestroyed += PlayerWins;
        _brickManager.OnBrickDestroyed += RandomSpawnPickUp;
        _menuManager.OnPauseMenuOpenClose += PausePlay;
        _pickUpManager.OnApplyPickUpBonus += ApplyPickUpBonus;
        _brickManager.BuildBricks();

        PaddleInit();

        BallsInit();

        _soundManager.SetBallControllersList(_ballControllersArray);
    }

    private void FillGameData()
    {
        switch (GameSettings._ballSpeed)
        {
            case GameSettings.BallSpeeds.Slow:
                _gameData._ballSpeed = 0.2f;
                break;
            case GameSettings.BallSpeeds.Normal:
                _gameData._ballSpeed = 0.5f;
                break;
            case GameSettings.BallSpeeds.Fast:
                _gameData._ballSpeed = 0.8f;
                break;
            default:
                _gameData._ballSpeed = 0.5f;
                break;
        }

        _gameData._playerLifesLeft = GameSettings._playerLifes;

        switch (GameSettings._paddleSize)
        {
            case GameSettings.PaddleSizes.Small:
                _gameData._paddleSize = 1.5f;
                break;
            case GameSettings.PaddleSizes.Normal:
                _gameData._paddleSize = 2.5f;
                break;
            case GameSettings.PaddleSizes.Big:
                _gameData._paddleSize = 3.5f;
                break;
            default:
                _gameData._paddleSize = 2.5f;
                break;
        }

        _gameData._multiball = GameSettings._multiball;
        _gameData._simultaneousBallNb = GameSettings._simultaneousBallNb;
        _gameData._paddleMovementSpeed = GameSettings._paddleMovementSpeed;
    }

    private void PaddleInit()
    {
        _paddleController.enabled = false;
        _paddleController.transform.localScale = new Vector3(_gameData._paddleSize, 0.5f, 0.5f);
        _paddleController.CalculateMovementLimits();
    }

    private void BallsInit()
    {
        _ballControllersArray.Clear();
        List<int> alreadyUsedIndexes = new List<int>();
        int ballNb = 1;
        if (_gameData._multiball)
            ballNb = _gameData._simultaneousBallNb;
        for (int i = 0; i < ballNb; i++)
        {
            bool indexSet = false;
            int spawnPosIndex = 0;
            while (!indexSet)
            {
                spawnPosIndex = Random.Range(0, _ballSpawnPositions.Count);
                if (!alreadyUsedIndexes.Contains(spawnPosIndex))
                {
                    alreadyUsedIndexes.Add(spawnPosIndex);
                    indexSet = true;
                }

            }

            GameObject ball = Instantiate(_ballPrefab);
            _ballControllersArray.Add(ball.GetComponent<BallController>());
            _ballControllersArray[i].Spawn(_gameData._multiball, _ballSpawnPositions[spawnPosIndex].position);
            _ballControllersArray[i].SetIndex(i);
            _ballControllersArray[i].SetSpeed(_gameData._ballSpeed);
            _ballControllersArray[i].OnBallOutOfBounds += BallOutOfBounds;
            _ballControllersArray[i].enabled = false;
        }
    }

    private void PausePlay(bool paused)
    {
        if (paused)
        {
            _timeScaleBeforePause = Time.timeScale;
            Time.timeScale = 0f;

            //apply pause to balls movement
            for (int i = 0; i < _ballControllersArray.Count; i++)
            {
                _ballControllersArray[i].SetTimeScale(0f);
            }
        }
        else
        {
            Time.timeScale = _timeScaleBeforePause;

            //restore balls movement to normal timescale
            for (int i = 0; i < _ballControllersArray.Count; i++)
            {
                _ballControllersArray[i].SetTimeScale(_timeScaleBeforePause);
            }
        }
    }

    private void RandomSpawnPickUp(Vector3 brickPos)
    {
        float spawnProba = Random.Range(0f, 1f);
        if(spawnProba > 0.5f)
        {
            _pickUpManager.RandomizePickUpSpawn(brickPos);
        }
    }

    private void ApplyPickUpBonus(PickUpSO.PickUpType type, float value, float duration)
    {
        switch (type)
        {
            case PickUpSO.PickUpType.bonusLife:
                _gameData._playerLifesLeft++;
                break;
            case PickUpSO.PickUpType.timeScale:
                if(!_isTimeScale)
                    StartCoroutine(TimeSlow(value, duration));
                break;
            case PickUpSO.PickUpType.paddleScale:
                if(!_isPaddleScale)
                    StartCoroutine(PaddleScale(value, duration));
                break;
            case PickUpSO.PickUpType.ballSpeed:
                if (!_isBallSpeed)
                    StartCoroutine(BallSpeed(value, duration));
                break;
            default:
                break;
        }
    }

    #region pick up bonuses
    IEnumerator TimeSlow(float timeScale, float duration)
    {
        Debug.Log("time slow");
        _isTimeScale = true;
        Time.timeScale = timeScale;
        //apply time scale to balls movement
        for (int i = 0; i < _ballControllersArray.Count; i++)
        {
            _ballControllersArray[i].SetTimeScale(timeScale);
        }
        yield return new WaitForSeconds(duration);
        Time.timeScale = 1f;
        //restore balls movement to normal timescale
        for (int i = 0; i < _ballControllersArray.Count; i++)
        {
            _ballControllersArray[i].SetTimeScale(1f);
        }
        _isTimeScale = false;
        Debug.Log("time normal");
    }

    IEnumerator PaddleScale(float scale, float duration)
    {
        Debug.Log("paddle size");
        _isPaddleScale = true;
        Vector3 defaultScale = _paddleController.transform.localScale;
        _paddleController.transform.localScale = new Vector3(defaultScale.x * scale, defaultScale.y, defaultScale.z);
        _paddleController.CalculateMovementLimits();
        yield return new WaitForSeconds(duration);
        _paddleController.transform.localScale = defaultScale;
        _paddleController.CalculateMovementLimits();
        _isPaddleScale = false;
        Debug.Log("paddle normal");
    }

    IEnumerator BallSpeed(float speed, float duration)
    {
        Debug.Log("ball speed");
        _isBallSpeed = true;
        for(int i = 0; i < _ballControllersArray.Count; i++)
        {
            _ballControllersArray[i].SetSpeed(speed);
        }
        yield return new WaitForSeconds(duration);
        for (int i = 0; i < _ballControllersArray.Count; i++)
        {
            _ballControllersArray[i].SetSpeed(_gameData._ballSpeed);
        }
        _isBallSpeed = false;
        Debug.Log("ball normal");
    }
    #endregion

    private void PlayerWins()
    {
        Debug.Log("PLAYER WINS");
        _paddleController.enabled = false;
        for(int i = 0; i < _ballControllersArray.Count; i++)
        {
            _ballControllersArray[i].enabled = false;
        }

        _soundManager.PlayStopBackgroundAudio(false);
        _soundManager.PlayWinAudio();
    }

    private void BallOutOfBounds(int ballIndex)
    {
        _gameData._playerLifesLeft--;
        if (_gameData._playerLifesLeft <= 0)
        {
            Debug.Log("PLAYER LOSES");
            _paddleController.enabled = false;

            for(int i = 0; i < _ballControllersArray.Count; i++)
            {
                _ballControllersArray[i].gameObject.SetActive(false);
            }

            _soundManager.PlayStopBackgroundAudio(false);
            _soundManager.PlayLoseAudio();
        }
        else
        {
            if(!_gameData._multiball)
                _soundManager.PlayStopBackgroundAudio(false);

            _soundManager.PlayOutOfBoundsAudio();
            RespawnBall(ballIndex);
        }
    }

    private void RespawnBall(int ballIndex)
    {
        int spawnPosIndex = Random.Range(0, _ballSpawnPositions.Count);

        _ballControllersArray[ballIndex].Spawn(_gameData._multiball, _ballSpawnPositions[spawnPosIndex].position);
        
        if(!_gameData._multiball)
        {
            _ballControllersArray[ballIndex].enabled = false;
            _gameStarted = false;
            _paddleController.Respawn();
            _paddleController.enabled = false;
        }
    }


    // Update is called once per frame
    void Update()
    {
        if(!_gameStarted)
        {
#if UNITY_STANDALONE || UNITY_EDITOR
            if(Input.GetMouseButtonDown(0))
            {
                _gameStarted = true;
                _paddleController.enabled = true;

                for(int i = 0; i < _ballControllersArray.Count; i++)
                {
                    _ballControllersArray[i].enabled = true;
                }

                _soundManager.PlayStopBackgroundAudio(true);
            }
#elif UNITY_ANDROID || UNITY_IOS
            if(Input.touchCount > 0)
            {
                _gameStarted = true;
                _paddleController.enabled = true;

                for(int i = 0; i < _ballControllersArray.Count; i++)
                {
                    _ballControllersArray[i].enabled = true;
                }

                _soundManager.PlayStopBackgroundAudio(true);
            }
#endif
        }

    }

    private void OnDestroy()
    {
        _brickManager.OnAllBricksDestroyed -= PlayerWins;
        _brickManager.OnBrickDestroyed -= RandomSpawnPickUp;
        _pickUpManager.OnApplyPickUpBonus -= ApplyPickUpBonus;
        _menuManager.OnPauseMenuOpenClose -= PausePlay;
        for (int i = 0; i < _ballControllersArray.Count; i++)
        {
            _ballControllersArray[i].OnBallOutOfBounds -= BallOutOfBounds;
        }
    }
}
