﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSettingsManager : MonoBehaviour
{
    [SerializeField]
    private Button[] _simultaneousBallsSelectionButtons;

    private void Start()
    {
        GameSettings._ballSpeed = GameSettings.BallSpeeds.Normal;
        GameSettings._paddleSize = GameSettings.PaddleSizes.Normal;
        GameSettings._playerLifes = 1;
        GameSettings._paddleMovementSpeed = 0.2f;
        GameSettings._multiball = false;
        GameSettings._simultaneousBallNb = 2;
    }

    public void PlayerLifeDown(TMPro.TextMeshProUGUI lifeSelectedText)
    {
        if (GameSettings._playerLifes > 1)
        {
            GameSettings._playerLifes--;
            lifeSelectedText.text = GameSettings._playerLifes.ToString();
        }
    }

    public void PlayerLifeUp(TMPro.TextMeshProUGUI lifeSelectedText)
    {
        if (GameSettings._playerLifes < 5)
        {
            GameSettings._playerLifes++;
            lifeSelectedText.text = GameSettings._playerLifes.ToString();
        }
    }

    public void BallSpeedDown(TMPro.TextMeshProUGUI speedSelectedText)
    {
        if (GameSettings._ballSpeed > GameSettings.BallSpeeds.Slow)
        {
            GameSettings._ballSpeed--;
            speedSelectedText.text = GameSettings._ballSpeed.ToString();
        }
    }

    public void BallSpeedUp(TMPro.TextMeshProUGUI speedSelectedText)
    {
        if (GameSettings._ballSpeed < GameSettings.BallSpeeds.Fast)
        {
            GameSettings._ballSpeed++;
            speedSelectedText.text = GameSettings._ballSpeed.ToString();
        }
    }

    public void PaddleSizeDown(TMPro.TextMeshProUGUI sizeSelectedText)
    {
        if (GameSettings._paddleSize > GameSettings.PaddleSizes.Small)
        {
            GameSettings._paddleSize--;
            sizeSelectedText.text = GameSettings._paddleSize.ToString();
        }
    }

    public void PaddleSizeUp(TMPro.TextMeshProUGUI sizeSelectedText)
    {
        if (GameSettings._paddleSize < GameSettings.PaddleSizes.Big)
        {
            GameSettings._paddleSize++;
            sizeSelectedText.text = GameSettings._paddleSize.ToString();
        }
    }

    public void MultiballOnOff(TMPro.TextMeshProUGUI multiballSelectedText)
    {
        GameSettings._multiball = !GameSettings._multiball;
        if(GameSettings._multiball)
        {
            multiballSelectedText.text = "Multiball On";
        }
        else
        {
            multiballSelectedText.text = "Multiball Off";
        }

        for(int i = 0; i < _simultaneousBallsSelectionButtons.Length; i++)
        {
            _simultaneousBallsSelectionButtons[i].interactable = GameSettings._multiball;
        }
    }

    public void SimultaneousBallsDown(TMPro.TextMeshProUGUI SimultaneousBallsSelectedText)
    {
        if (GameSettings._simultaneousBallNb > 2)
        {
            GameSettings._simultaneousBallNb--;
            SimultaneousBallsSelectedText.text = GameSettings._simultaneousBallNb.ToString();
        }
    }

    public void SimultaneousBallsUp(TMPro.TextMeshProUGUI SimultaneousBallsSelectedText)
    {
        if (GameSettings._simultaneousBallNb < 3)
        {
            GameSettings._simultaneousBallNb++;
            SimultaneousBallsSelectedText.text = GameSettings._simultaneousBallNb.ToString();
        }
    }
}
